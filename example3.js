function printUnorderedPairs(array){
  for(let i = 0; i < array.length; i++){        //O(n)
    for(let j = i + 1; j < array.length; j++){  //O(*n)
      console.log(`${array[i]}, ${array[j]}`);  //O(1)
    }
  }
}

//¿Cuál es la complejidad de tiempo?
//O(n*n + 1)
//O(n^2)
function reverse(array){
  for(let i = 0; i < array.length / 2; i++){//O(n/2)
      let other = array.length - i - 1;     //O(n/2)
      let temp = array[i];                  //O(n/2)
      array[i] = array[other];              //O(n/2)
      array[other] = temp;                  //O(n/2)
  }
}

//¿Cuál es la complejidad de tiempo?
//O(2n/2)
//O(n)
function printUnorderedPairs(arrayA, arrayB){
  for(let i = 0; i < arrayA.length; i++){           //O(n)
    for(let j = 0; j < arrayB.length; j++){         //O(m)
      for(let k = 0; k < 100000; k++){              //O(100000)
        console.log(`${arrayA[i]}, ${arrayB[j]}`);  //0(100000)
      }
    }
  }
}

//¿Cuál es la complejidad de tiempo?
//O(n*m + 200000)
//O(nm)

function printUnorderedPairs(arrayA, arrayB){
  for(let i = 0; i < arrayA.length; i++){           //O(n)
    for(let j = 0; j < arrayB.length; j++){         //O(m)
      if(arrayA[i] < arrayB[j]){                    //O(m)
        console.log(`${arrayA[i]}, ${arrayB[j]}`);  //O(m)
      }
    }
  }
}

//¿Cuál es la complejidad de tiempo?
//O(n * 3m)
//O(3nm)
//O(nm)
